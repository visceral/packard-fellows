// Polyfill to make forEach on querySelectorAll work in IE
if (typeof NodeList !== "undefined" && NodeList.prototype && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}
// Polyfill because IE doesn't support remove so we replace it with removeChild
if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function() {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}
// Polyfill to make IE support startsWith
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position){
      position = position || 0;
      return this.substr(position, searchString.length) === searchString;
  };
}


// Setup variables
var container, stats;
var camera, controls, scene, renderer, raycaster, mouse;

var nodeCluster = new THREE.Object3D();
var starField;
var nodes = [];
var introActive = true;
var tourNodes = [];
var tourActive = false;
var fellowOverlayActive = false;
var filterActive = false;
var filterNodeCount = 0;
var textLabels = [];
var pickedNode;
var intersectedNode;
var posts = [];

var regularNodeScale = 1;
var regularNodeColor = 0xA3FAFE;
var quoteNodeScale = 2.5;
var quoteNodeColor = 0x00B9C2;
var storyNodeScale = 10;
var storyNodeColor = 0x00E7F2;

var mouse = new THREE.Vector2();
var offset = new THREE.Vector3( 10, 10, 10 );

var sound = true;

window.addEventListener("load", function() {
    var testCanvas = document.createElement("canvas");
    testCanvas.id = 'test-canvas';
    // Get WebGLRenderingContext from canvas element.
    var gl = testCanvas.getContext("webgl") || testCanvas.getContext("experimental-webgl");
    // Report the result.
    if (gl && gl instanceof WebGLRenderingContext) {
        // Fellows data
        // var url = 'https://dev-packard2.thisisvisceral.com/wp-json/fellows_endpoint/v1/fellows';
        var url = 'https://www.packard.org/wp-json/fellows_endpoint/v1/fellows/?v=2019';
        if (getParameterByName('local') == 'true') {
            url = location.origin + '/data/fellows.json';
        }
        // Create nodes
        loadFellows(url);
        loadFilters();
    } else {
        var newLoader = document.getElementById('loading__inner');
        newLoader.innerHTML = '<p class="text-center">This experience is meant for modern browsers, and unfortunately yours is outdated. Visit the <a href="https://www.packard.org/what-we-fund/science/packard-fellowships-for-science-and-engineering/">Packard Foundation website</a> to learn more about the Fellowship for Science and Engineering.</p>';
    }
  }, false);





// Load in Fellows Data
function loadFellows(url) {
    var posts = [];
    var xhr = new XMLHttpRequest();
    xhr.addEventListener('progress', updateProgress);
    xhr.addEventListener('load', loadComplete);
    xhr.open('GET', url, true);
    xhr.responseType = 'text';
    if (!xhr) {
        alert('Your browser does not support XMLHttpRequest');
        return;
    }

    xhr.onreadystatechange = function() {
        if (xhr.readyState==4 && xhr.status==200) {
            var fellowsJSON = JSON.parse(xhr.responseText);
    
            for (i = 0; i < fellowsJSON.length; i++) {
                window['posts'][i] = { 
                    objectId: i,
                    name: fellowsJSON[i].name,
                    slug: fellowsJSON[i].slug,
                    image: fellowsJSON[i].image,
                    year: fellowsJSON[i].year,
                    institution: fellowsJSON[i].institution,
                    quote: fellowsJSON[i].quote,
                    extendedQuote: fellowsJSON[i].extended_quote,
                    highlightStory: fellowsJSON[i].highlight_story,
                    advisoryPanelMember: fellowsJSON[i].advisory_panel_member[0],
                    discipline: fellowsJSON[i].disciplines,
                    bio: fellowsJSON[i].bio
                };
            }
        }
    };
    xhr.onload = function() {
        if (xhr.status==404 || xhr.status==500) {
            window.location.href = "/?local=true";
        }
    };
    xhr.onerror = function() {
        alert('Woops, there was an error making the request.');
    };
    xhr.send();
    
    function updateProgress (oEvent) {
        document.querySelector('#loading-progress p').innerHTML = Math.round((oEvent.loaded / 829142)* 100) + '%';
    }

    function loadComplete(oEvent) {
        document.querySelector('.loading').style.display = 'none';
        document.querySelector('.intro-overlay').style.display = 'block';
        // Initate Three.js scene and start animation
        init();
        animate();
        
        if (getCookie('packard_fellows_info_clicked')) {
            document.querySelector('.info').className  += ' info--stopped';
        }
    }
}

// Load in Filters Data
function loadFilters() {
    // var url = 'https://dev-packard2.thisisvisceral.com/wp-json/fellows_endpoint/v1/filters';
    var url = 'https://www.packard.org/wp-json/fellows_endpoint/v1/filters/?v=2019';
    if (getParameterByName('local') == 'true') {
        url = location.origin + '/data/filters.json';
    }
    var posts = [];
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'text';
    if (!xhr) {
        alert('Your browser does not support XMLHttpRequest');
        return;
    }

    xhr.onreadystatechange = function() {
        if (xhr.readyState==4 && xhr.status==200) {
            var filtersJSON = JSON.parse(xhr.responseText);

            var disciplines = filtersJSON['disciplines'];
            var filterDisciplines = document.getElementById('discipline-filter');
            for (i = 0; i < disciplines.length; i++) {
               var option = document.createElement('option');
               option.text = disciplines[i];
               option.value = disciplines[i];
               filterDisciplines.add(option);
            }

            var institutions = filtersJSON['institutions'];
            var filterInstitutions = document.getElementById('institution-filter');
            for (i = 0; i < institutions.length; i++) {
               var option = document.createElement('option');
               option.text = institutions[i];
               option.value = institutions[i]; 
               filterInstitutions.add(option);
            }

            var years = filtersJSON['years'];
            var filterYears = document.getElementById('year-filter');
            for (i = 0; i < years.length; i++) {
               var option = document.createElement('option');
               option.text = years[i];
               option.value = years[i];
               filterYears.add(option);
            }
        }
    };
    xhr.onload = function() {
        if (xhr.status==404 || xhr.status==500) {
            window.location.href = "/?local=true";
        }
    };
    xhr.onerror = function() {
        alert('Woops, there was an error making the request.');
    };
    xhr.send();
}

// Open node overlay function
function openOverlay(overlay) {
    document.querySelector('body').className = 'hide-ui';
    removeTextLabels();
    if (overlay === 'fellow-overlay') {
        fellowOverlayActive = true;
    }
    var element = document.getElementById(overlay);
    // console.log(element);
    // fadeIn(element, 2000);
    element.classList.add('overlay--open');
    var anchorLinks = document.querySelectorAll('a');
    // if (anchorLinks !== null ) {
        anchorLinks.forEach(function(el) {
            el.addEventListener('mousedown', hashLinks, false);
        });
    // }
    responsiveIframes();
  
}

// Sets iframes to be responsive
function responsiveIframes() {
    var iframes = document.querySelectorAll('.overlay iframe');
    // console.log(iframes);
    // Only add wrapper if it's a youtube iframe
    iframes.forEach(function(el) {
        wrapper = document.createElement('div')
        wrapper.classList += 'iframe-wrap';
        el.parentNode.insertBefore(wrapper, el);
        wrapper.appendChild(el);

    });
}

// Open info overlay
document.querySelector('.info').addEventListener('mousedown', function() {
    // Universal Analytics
    gtag('event', 'Click', {
        'event_category' : 'UI',
        'event_label' : 'Info'
    });
    // Google Analytics 4
    gtag('event', 'UI', {
        'Info' : 'Click',
    });
    openOverlay("info-overlay");
}, false);

// Stop info pulse once it's been clicked once
document.querySelector('.info').addEventListener('mousedown', stopInfoPulse, false);

// Open help overlay
document.querySelector('.help').addEventListener('mousedown', function() {
    // Universal Analytics
    gtag('event', 'Click', {
        'event_category' : 'UI',
        'event_label' : 'Help'
    });
    // Google Analytics 4
    gtag('event', 'UI', {
        'Help' : 'Click',
    });
    openOverlay("help-overlay");
}, false);

function closeOverlay() {
    document.querySelector('body').className = 'scene-entered';
    var element = document.querySelectorAll('.overlay');
    // fadeOut(element, 2000);
    element.forEach(function(el) {
        el.classList.remove('overlay--open');
    });

    // Fades in the picked node and resets the pickedNode variable
    if (fellowOverlayActive == true) {
        // Only fade in if the node is active
        if (pickedNode.userData.active === true) {
            fadeMesh(pickedNode, 'in');
        // If it's inactive, fade out to the semi-transparent 
        } else {
            fadeMesh(pickedNode, 'out');
        }
        cameraReset();
        nodePositionReset();
        removeHashUrl();
        pickedNode = false;
        // Clears content of fellow overlay single fellow
        document.querySelector('.single-fellow').innerHTML = '';
        audioPlay('panel-close');
        fellowOverlayActive = false;
    }
    
    controls.autoRotate = true;

    if (tourActive) {
        tourActive = false;
        if (document.getElementById('prev-link')) {
            document.getElementById('fellow-overlay').removeChild(document.getElementById('prev-link'));
        }
        if (document.getElementById('next-link')) {
            document.getElementById('fellow-overlay').removeChild(document.getElementById('next-link'));
        } 
    }
}

function removeIntro() {  
    var element = document.querySelector('.intro-overlay');
    element.style.display = 'none';
}

// Close overlay 
document.querySelectorAll('.overlay__overlay-close').forEach(function(el) {
    el.addEventListener('mousedown', closeOverlay, false);
});

document.getElementById('tour').addEventListener('mousedown', nodeTour, false);

function nodeTour() {
    // Universal Analytics
    gtag('event', 'Click', {
        'event_category' : 'UI',
        'event_label' : 'Tour'
    });
    // Google Analytics 4
    gtag('event', 'UI', {
        'Tour' : 'Click',
    });

    tourActive = true;
    var firstTourNode = tourNodes[0];
    pickedNode  = firstTourNode;
    populateNodeOverlay(firstTourNode);				
    cameraMove(firstTourNode);
    pickedNodeMove(firstTourNode);
    randomNodeDispersal(firstTourNode);
    addHashUrl(firstTourNode);  
}
			
// START INIT
function init() {
    container = document.getElementById( 'container' );
    renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    container.appendChild( renderer.domElement );

    camera = new THREE.PerspectiveCamera( 10, window.innerWidth / window.innerHeight, 1, 10000 );
    
    camera.position.x = 5800;
    camera.position.y = 5800;
    camera.position.z = 5800;

    controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.autoRotateSpeed = 0.05;
    controls.rotateSpeed = 0.35;
    controls.zoomSpeed = 0.75;
    controls.enablePan = false;
    controls.enableZoom = true;
    // controls.staticMoving = true;
    controls.autoRotate = true;
    controls.enableDamping;
    controls.DampingFactor = 25;
    controls.maxDistance = 3250;
    // controls.target = new THREE.Vector3(camera.position.x,camera.position.y,camera.position.z - 2400);

    scene = new THREE.Scene();

    // Used for determining a point in 3-D space when someone clicks on the screen
    raycaster = new THREE.Raycaster();

    // Event listeners for filters which will track events in Google Analytics
    document.getElementById('name-filter').addEventListener('keypress', function() {
        if (event.which == 13 || event.keyCode == 13) {
            if (document.getElementById('name-filter').value != '') {
                // Universal Analytics
                gtag('event', 'Name', {
                    'event_category' : 'Filters',
                    'event_label' : document.getElementById('name-filter').value
                });
                // Google Analytics 4
                gtag('event', 'Filters', {
                    'Name' : document.getElementById('name-filter').value,
                });
            }
            filterNodes();
        }
    });
    document.getElementById('year-filter').addEventListener('change', function() {
        if (document.getElementById('year-filter').options[document.getElementById('year-filter').selectedIndex].value != '') {
            // Universal Analytics
            gtag('event', 'Year', {
                'event_category' : 'Filters',
                'event_label' : document.getElementById('year-filter').options[document.getElementById('year-filter').selectedIndex].value
            });
            // Google Analytics 4
            gtag('event', 'Filters', {
                'Year' : document.getElementById('year-filter').options[document.getElementById('year-filter').selectedIndex].value
            });
        }
        filterNodes();
    });
    document.getElementById('discipline-filter').addEventListener('change', function() {
        if (document.getElementById('discipline-filter').options[document.getElementById('discipline-filter').selectedIndex].value != '') {
            // Universal Analytics
            gtag('event', 'Discipline', {
                'event_category' : 'Filters',
                'event_label' : document.getElementById('discipline-filter').options[document.getElementById('discipline-filter').selectedIndex].value
            });
            // Google Analytics 4
            gtag('event', 'Filters', {
                'Year' : document.getElementById('discipline-filter').options[document.getElementById('discipline-filter').selectedIndex].value
            });
        }
        filterNodes();
    }); 
    document.getElementById('institution-filter').addEventListener('change', function() {
        if (document.getElementById('institution-filter').options[document.getElementById('institution-filter').selectedIndex].value != '') {
            // Universal Analytics
            gtag('event', 'Institution', {
                'event_category' : 'Filters',
                'event_label' : document.getElementById('institution-filter').options[document.getElementById('institution-filter').selectedIndex].value
            });
            // Google Analytics 4
            gtag('event', 'Filters', {
                'Institution' : document.getElementById('institution-filter').options[document.getElementById('institution-filter').selectedIndex].value
            });
        }
        filterNodes();
    });  
    document.getElementById('clear-filter').addEventListener('click', function() {
        // Universal Analytics
        gtag('event', 'Institution', {
            'event_category' : 'Filters',
            'event_label' : document.getElementById('institution-filter').options[document.getElementById('institution-filter').selectedIndex].value
        });
        // Google Analytics 4
        gtag('event', 'Filters', {
            'Institution' : document.getElementById('institution-filter').options[document.getElementById('institution-filter').selectedIndex].value
        });
        clearFilters();
    });
    function clearFilters() {
        document.getElementById('name-filter').value = '';
        document.getElementById('year-filter').options.selectedIndex = 0;
        document.getElementById('institution-filter').options.selectedIndex = 0;
        document.getElementById('discipline-filter').options.selectedIndex = 0;
        filterNodes();
    }

    function filterNodes() {
        audioPlay('filter');

        var name = document.getElementById('name-filter').value;
        var year = document.getElementById('year-filter').options[document.getElementById('year-filter').selectedIndex].value;
        var institution = document.getElementById('institution-filter').options[document.getElementById('institution-filter').selectedIndex].value;
        var discipline = document.getElementById('discipline-filter').options[document.getElementById('discipline-filter').selectedIndex].value;
        // console.log(name);
        // console.log(year);
        // console.log(institution);
        // console.log(discipline);

        // Reset nodes that are inactive (transparent)
        var inactiveNodes = 0;
        // Reset count of filtered nodes (for determining radius of filtered cluster)
        filterNodeCount = 0;
        if (name == '' && year == '' && institution == '' && discipline == '') {
            filterActive = false;
            inactiveNodes = 0;
            for (i=0; i < nodeCluster.children.length; i++) {
                fadeMesh(nodeCluster.children[i], 'in');
                nodeCluster.children[i].userData.active = true;
            }
            nodePositionReset();
        } else {
            filterActive = true;
            var updatedNodePoints = [];

            for (i=0; i < nodeCluster.children.length; i++) {
                if (filterNodeCheck(nodeCluster.children[i], name, year, institution, discipline) ) {
                    filterNodeCount++;
                }
            }
            var clusterR = (filterNodeCount * 5) * 0.75;
            if (clusterR < 30) {
                clusterR = 30;
            } else if (clusterR > 200) {
                clusterR = 200;
            }

            for (i=0; i < nodeCluster.children.length; i++){ 
                if (filterNodeCheck(nodeCluster.children[i], name, year, institution, discipline) ) {
                    fadeMesh(nodeCluster.children[i], 'in');
                    singleNodeCluster(nodeCluster.children[i], clusterR);
                    
                    nodeCluster.children[i].userData.active = true;
                    updatedNodePoints.push(nodeCluster.children[i].position);
                    // console.log(nodeCluster.children[i]);
                } else {
                    inactiveNodes++;
                    fadeMesh(nodeCluster.children[i], 'out');
                    randomSingleNodeDispersal(nodeCluster.children[i]);
                    
                    nodeCluster.children[i].userData.active = false;
                }
            }
        }

        if (inactiveNodes === nodeCluster.children.length) {
            document.querySelector('.filter-message').classList.add('filter-message--active');
        } else {
            document.querySelector('.filter-message').classList.remove('filter-message--active');
        }
        
        // Hides the keyboard on mobile
        document.getElementById('name-filter').blur(); 
    }

    function filterNodeCheck(el, name, year, institution, discipline) {
        if ((name == '' || el.userData.name.toLowerCase().indexOf(name.toLowerCase()) != -1) && 
                    (year == '' || el.userData.year == year) &&
                    (institution == '' || el.userData.institution == institution) &&
                    (discipline == '' || el.userData.discipline.indexOf(discipline) != -1) ) {
            return true;
        } else {
            return false;
        }              
    }

    document.getElementById('container').addEventListener('mousedown', onDocumentMouseDown, true);
    document.getElementById('container').addEventListener('touchstart', onDocumentMouseDown, true);
    document.getElementById('container').addEventListener('mousemove', onDocumentMouseMove, true);

    var nodePoints = [];

    posts.forEach (function(el) {
        var geometry = new THREE.CircleBufferGeometry( 2, 64 );

        // Set node color
        var circleColor;
        var positionRadius = 1000;
        if (el.highlightStory) {
            circleColor = storyNodeColor;
            positionRadius = 500;
        } else if (el.quote) {
            circleColor = quoteNodeColor;
        } else {
            circleColor = regularNodeColor;
        }
        // var cr = Math.random();
        // if (cr < 0.33) {
        //     circleColor = 0x5BD7DD;
        // } else if (cr > 0.67) {
        //     circleColor = 0x1596A3;
        // } else {
        //     circleColor = 0xffffff;
        // }

        var material = new THREE.MeshBasicMaterial( { color: circleColor, transparent: true } );

        // set random position inside an oblate spheroid (flat sphere)
        // Set variables for math
        var u = Math.random();
        var v = Math.random();
        var r = positionRadius * Math.sqrt(Math.random());
        var theta = 2 * Math.PI * u;
        var phi = Math.acos(2 * v - 1);
       
        // Set positions based on math
        var position = new THREE.Vector3();
        position.x = (r * Math.sin(phi) * Math.cos(theta));
        position.y = ((r/2) * Math.sin(phi) * Math.sin(theta));
        position.z = (r * Math.cos(phi));

        // Create node
        var node = new THREE.Mesh( geometry, material );

        // Set node position to randomized postion coordinates
        node.position.x = position.x;
        node.position.y = position.y;
        node.position.z = position.z;

        // Add node position to nodePoints array
        nodePoints.push(node.position);

        // Set node size (based on if they have a story, quote, or not)
        if (el.highlightStory) {
            nodeScale = storyNodeScale;
            addGlow(node, 0x1596A3, true);
        } else if (el.quote) {
            nodeScale = quoteNodeScale;
            addGlow(node, 0x1596A3, false);
        } else {
            nodeScale = regularNodeScale;
        }
        node.scale.set(nodeScale, nodeScale, nodeScale);

        // Set up custom user data
        node.userData = {
            id: el.objectId, 
            name: el.name, 
            slug: el.slug, 
            image: el.image,
            year: el.year, 
            institution: el.institution, 
            discipline: el.discipline, 
            bio: el.bio,
            quote: el.quote,
            extendedQuote: el.extendedQuote,
            highlightStory: el.highlightStory,
            advisoryPanelMember: el.advisoryPanelMember,
            originalPosition: position, 
            filterPosition: null, 
            selected: false, 
            active: true,
            textLabelActive: false
        };
           
        // Add node to nodeCluster group
        nodeCluster.add(node);

        // Add node to nodes array
        nodes.push(node);

        // Add node to node highlights array
        if (el.highlightStory) {
            tourNodes.push(node);
        }
    });

    // After we've created and added all the nodes to the group, add the group to the scene
    scene.add( nodeCluster );

    // console.log(textLabels);
    // console.log(tourNodes);

    // Add 3 star fields of various sizes
    addStarField(10);
    addStarField(20);
    addStarField(30);
    
    // Stats for testing
    // stats = new Stats();
    // container.appendChild( stats.dom );

    // Listen for window resize and adjust WebGL scene acordingly
    window.addEventListener('resize', function() {
        var WIDTH = window.innerWidth,
        HEIGHT = window.innerHeight;
        renderer.setSize(WIDTH, HEIGHT);
        camera.aspect = WIDTH / HEIGHT;
        camera.updateProjectionMatrix();
    });
}
// END INIT
        
// START ONDOCUMENTMOUSEDOWN
function onDocumentMouseDown(e) {
    e.preventDefault();
   
    var targetX, targetY;
    if (e.clientX !== undefined) {
        targetX = e.clientX;
        targetY = e.clientY;
    } else if (e.touches[0].pageX !== undefined) {
        targetX = e.touches[0].pageX;
        targetY = e.touches[0].pageY;
    }
    // console.log(targetX + ", " + targetY);
    mouse.x = ( targetX / renderer.domElement.clientWidth ) * 2 - 1;
    mouse.y = - ( targetY / renderer.domElement.clientHeight ) * 2 + 1;
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( nodes, true );

    if ( intersects.length > 0 ) {
        if ( intersects[ 0 ].object.userData.active == true ) {
            pickedNode = intersects[ 0 ].object;
            // Universal Analytics
            gtag('event', 'Click', {
                'event_category' : 'Fellows',
                'event_label' : intersects[ 0 ].object.userData.name
            });
            // Google Analytics 4
            gtag('event', 'Fellows', {
                'Click' : intersects[ 0 ].object.userData.name
            });
            audioPlay('panel-open');

            removeTextLabels()
            populateNodeOverlay(intersects[ 0 ].object);				
            cameraMove(intersects[ 0 ].object);
            pickedNodeMove(intersects[ 0 ].object);
            randomNodeDispersal(intersects[ 0 ].object);
            addHashUrl(intersects[ 0 ].object);     
        }   
    } 
}

// END ONDOCUMENTMOUSEDOWN

function onDocumentMouseMove(e) {
    mouse.x = ( e.clientX / renderer.domElement.clientWidth ) * 2 - 1;
    mouse.y = - ( e.clientY / renderer.domElement.clientHeight ) * 2 + 1;
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( nodes, true );

    // If the mouse is hovering over a node
    if ( intersects.length > 0 ) {
        // If the hovered node is the same node that was just previously being hovered over
        if (intersectedNode !== intersects[0].object.userData.slug) {
            // console.log('hovering over: ' + intersects[0].object);
            // If the node is active (not transparent because of a filter)
            // console.log('hovering over ' + intersectedNode);
            if ( intersects[0].object.userData.active == true ) {
                // Go through each node and set the previously hovered node's textLabelActive to false
                nodeCluster.children.forEach(function(node) {
                    if (node.userData.slug === intersectedNode) {
                        node.userData.textLabelActive = false;
                    }
                });
                document.getElementById('container').style.cursor = 'pointer';
                // Set the interesected node variable to this new node
                intersectedNode =  intersects[0].object.userData.slug;
                
                removeTextLabels();

                // If node doesn't have a text label, set that value to true and add text label
                if (intersects[0].object.userData.textLabelActive === false && intersects[0].object.userData.active === true) {
                    // console.log(intersects[0].object.userData.name);
                    intersects[0].object.userData.textLabelActive = true;
                    
                    // Add text label
                    var textLabel = createTextLabel(intersects[0].object);
                    textLabels.push(textLabel);
                    container.appendChild(textLabel.element);
                    // console.log(textLabels);
                }
            }
        } 
    // Else, no node is being hovered over, so clear text labels array (for calculating label position), 
    // remove all label elements, and set all node textLabelActive properties to false 
    } else {
        intersectedNode = '';
        document.getElementById('container').style.cursor = 'auto';
        
        removeTextLabels();

        nodeCluster.children.forEach(function(node) {
            if (node.userData.textLabelActive === true) {
                node.userData.textLabelActive = false;
            }
        }); 
    }
}

// START ADDSTARFIELD
function addStarField(size) {
    var pointSprite = new THREE.TextureLoader().load( 'images/circle-2.png' );
    // Create star field
    var pointMaterial = new THREE.PointsMaterial({
        color: 0xffffff,
        size: size, 
        opacity: 1,
        map: pointSprite,
        transparent: true
    });

    var pointGeometry = new THREE.Geometry();
    starField = new THREE.Points(pointGeometry, pointMaterial);

    // Create random points for each star (vertex of the point geometry)
    for (var i = 0; i < 12500; i++) {
        var u = Math.random();
        var v = Math.random();
        var r = 3500;
        var theta = 2 * Math.PI * u;
        var phi = Math.acos(2 * v - 1);
       
        // Set positions based on math
        var pointPosition = new THREE.Vector3();
        pointPosition.x = (r * Math.sin(phi) * Math.cos(theta));
        pointPosition.y = (r * Math.sin(phi) * Math.sin(theta));
        pointPosition.z = (r * Math.cos(phi));

        // Add random points to point geometry vertices
        pointGeometry.vertices.push(pointPosition);
    }

    // Add star field to the scene
    scene.add(starField);
}
// END ADDSTARFIELD

// START ADDGLOW
function addGlow(node, color, pulse) {
    var spriteMaterial = new THREE.SpriteMaterial({ 
        map: new THREE.TextureLoader().load( 'images/glow.png' ), 
        color: color, transparent: false, blending: THREE.AdditiveBlending
    });
    var sprite = new THREE.Sprite( spriteMaterial );
    sprite.position = node.position;
    sprite.position.z = (sprite.position.z - 1); // Position the glow sprite behind the node to prevent blending issue
    sprite.scale.set(7.5, 7.5, 1.0);
    node.add(sprite);

    if (pulse == true) {
        var spread = Math.random();
        if (spread > 0.5) spread = spread - 0.5;
        var speed = Math.random() + 1;
        glowPulse(node, spread, speed);
    }
}
// END ADDGLOW

// START GLOWPULSE
function glowPulse(node, percentage, speed) {	
    var tweenPulse = new TWEEN.Tween( node.children[0].scale ).to( {
        x: (node.children[0].scale.x + (node.children[0].scale.x * percentage)),
        y: (node.children[0].scale.y + (node.children[0].scale.y * percentage)),
        z: 1.0}, (speed * 1000) )
        .easing( TWEEN.Easing.Quadratic.InOut)
        .repeat(Infinity)
        .yoyo(true)
        .start();
    return tweenPulse;
}
// END GLOWPULSE

// START FADEMESH
function fadeMesh(mesh, direction, options) {			
    options = options || {};
    
    // set and check 
    // this check is used to work with normal and multi materials.
    var mats = mesh.material.materials ? mesh.material.materials : [mesh.material],
    easing = options.easing || TWEEN.Easing.Linear.None,
    duration = options.duration || 1000;
    
    var current = { percentage : mats[0].opacity };
            
    // tween opacity back to originals
    var tweenOpacity = new TWEEN.Tween(current)
        .to({ percentage: direction == 'out' ? 0.1 : 1 }, duration)
        .easing(easing)
        .onUpdate(function() {
            for (var i = 0; i < mats.length; i++) {
                mats[i].opacity = current.percentage; // Update the object opacity
                if (mesh.children[0]) mesh.children[0].material.opacity = current.percentage; // If object has glow, update that as well.
            }
        })
        .onComplete(function(){
            if(options.callback){
                options.callback();
            }
        });
    tweenOpacity.start();
    return tweenOpacity;
}
// END FADEMESH

function pickedNodeMove(node) {
    // console.log(node);
    controls.autoRotate = false;
    var nodePosition = node.position;
    var nodeOpacity = node.material.opacity;
    // console.log(nodeOpacity);

    var moveNodePosition = new TWEEN.Tween( nodePosition ).to( {
            x: 0,
            y: 0,
            z: 0}, 1500 )
        .easing( TWEEN.Easing.Quadratic.InOut)
        .onComplete(function(){
            var mats = node.material.materials ? node.material.materials : [node.material],
            easing = TWEEN.Easing.Linear.None,
            duration = 300;
            
            var current = { percentage : mats[0].opacity };
                    
            // tween opacity back to originals
            var tweenOpacity = new TWEEN.Tween(current)
                .to({ percentage: 0 }, duration)
                .easing(easing)
                .onUpdate(function() {
                    for (var i = 0; i < mats.length; i++) {
                        mats[i].opacity = current.percentage; // Update the object opacity
                        if (node.children[0]) node.children[0].material.opacity = current.percentage; // If object has glow, update that as well.
                    }
                })
            tweenOpacity.start();
            
            return tweenOpacity;   
        });
        
    moveNodePosition.start();
    return moveNodePosition;             
}

function cameraIntro() {	
    document.getElementById('background-audio').play();
    audioPlay('intro');

    var cameraTween = new TWEEN.Tween( camera.position ).to( {
        x: 0,
        y: 0,
        z: 1200}, 2500 )
        .easing( TWEEN.Easing.Quadratic.InOut).start();
    return cameraTween;
}

function randomNodeDispersal(node) {
    var nodeID = node.uuid;

    // Go through each node
    nodeCluster.children.forEach(function(el) {
        // if the node is not the node selected, create a random position for it 
        if (el.uuid !== nodeID ) {
            var randomX = Math.random() < 0.5 ? -1 : 1;
            var randomY = Math.random() < 0.5 ? -1 : 1;
            var randomZ = Math.random() < 0.5 ? -1 : 1;

            var position = new THREE.Vector3();
            position.x = Math.random() * 1000 * randomX;
            position.y = Math.random() * 1000 * randomY;
            position.z = Math.random() * 1000 * randomZ;

            // Animate the node to that newly created randowm position
            var moveNodePosition = new TWEEN.Tween( el.position ).to( {
                x: position.x,
                y: position.y,
                z: position.z}, 700 )
            .easing( TWEEN.Easing.Quadratic.InOut).start()
            .onComplete(function(){
                // console.log(nodeID);
            });
            
            moveNodePosition.start();
            return moveNodePosition; 
        }    
    });
}

function randomSingleNodeDispersal(node) {
    var randomX = Math.random() < 0.5 ? -1 : 1;
    var randomY = Math.random() < 0.5 ? -1 : 1;
    var randomZ = Math.random() < 0.5 ? -1 : 1;

    var position = new THREE.Vector3();
    position.x = Math.random() * 500 * randomX;
    position.y = Math.random() * 500 * randomY;
    position.z = Math.random() * 500 * randomZ;

    node.userData.filterPosition = position;

    var moveNodePosition = new TWEEN.Tween( node.position ).to( {
        x: position.x,
        y: position.y,
        z: position.z}, 700 )
    .easing( TWEEN.Easing.Quadratic.InOut).start()
    .onComplete(function(){
    });
    
    moveNodePosition.start();
    return moveNodePosition; 
}  

function singleNodeCluster(node, radius) {
    var u = Math.random();
    var v = Math.random();
    var r = radius * Math.sqrt(Math.random());
    var theta = 2 * Math.PI * u;
    var phi = Math.acos(2 * v - 1);

    var position = new THREE.Vector3();
    position.x = (r * Math.sin(phi) * Math.cos(theta));
    position.y = (r * Math.sin(phi) * Math.sin(theta));
    position.z = (r * Math.cos(phi));

    node.userData.filterPosition = position;

    var moveNodePosition = new TWEEN.Tween( node.position ).to( {
        x: position.x,
        y: position.y,
        z: position.z}, 700 )
    .easing( TWEEN.Easing.Quadratic.InOut).start();
}  

// resets randomly dispersed nodes to original position
function nodePositionReset() {
    // Go through each node
    nodeCluster.children.forEach(function(el) {
        // Animate node to original or filtered position
        var resetPosition = el.userData.originalPosition;

        if (filterActive) {
            resetPosition = el.userData.filterPosition;
        }

        var moveNodePosition = new TWEEN.Tween( el.position ).to( {
            x: resetPosition.x,
            y: resetPosition.y,
            z: resetPosition.z}, 1500 )
        .easing( TWEEN.Easing.Quadratic.InOut).start();

        moveNodePosition.start();
        return moveNodePosition; 
    });
}

// Move camera to a specific node
function cameraMove() {
    document.querySelector('body').className = 'hide-ui';
    var moveCameraPosition = new TWEEN.Tween( camera.position ).to( {
            x: mouse.x,
            y: mouse.y,
            z: 100}, 1400 )
        .easing( TWEEN.Easing.Quadratic.InOut).start()
        .onComplete(function(){
            openOverlay('fellow-overlay');
            // Debugging
            // console.log(camera.position);  
        });
        
    moveCameraPosition.start();
    return moveCameraPosition;  
}

function fellowOverlayCameraMove() {
    var moveCameraPosition = new TWEEN.Tween( camera.position ).to( {
        x: camera.position.x,
        y: camera.position.y,
        z: 1750}, 1000 )
    .easing( TWEEN.Easing.Quadratic.InOut).start()
    .onComplete(function(){
        cameraMove();
    });
    
    moveCameraPosition.start();
    return moveCameraPosition;  
}

// Adds node data to the overlay
function populateNodeOverlay(node) {
    var nodeImage = node.userData.image;
    var nodeName = node.userData.name;
    var nodeSlug = node.userData.slug;
    var nodeInstitution = node.userData.institution;
    var nodeYear = node.userData.year;
    var nodeDiscipline = node.userData.discipline;
    var nodeBio = node.userData.bio;
    var nodeQuote = node.userData.quote;
    var nodeExtendedQuote = node.userData.extendedQuote;
    var nodeHighlightStory = node.userData.highlightStory;

    document.getElementById('fellow-overlay').scrollTop = 0;

    var htmlOutput = '';

    htmlOutput += '<div class="single-fellow">';
    if (nodeImage) {
        htmlOutput += '<img class="single-fellow__image" src="' + nodeImage + '">';
    }
    if (nodeName) {
        htmlOutput += '<h1 class="single-fellow__title text-center">' + nodeName + '</h1>';
    }
    htmlOutput += '<div class="single-fellow__meta text-center">';
    if (nodeYear) {
        htmlOutput += '<h4>' + nodeYear + ' Fellow' + '</h4>';
    }
    if (nodeInstitution || nodeDiscipline) {
        htmlOutput += '<h4>';
    }    
    if (nodeInstitution.length > 0) {
        htmlOutput += nodeInstitution;
    }
    if (nodeDiscipline.length > 0) {
        htmlOutput += ' • '; + nodeDiscipline;
        nodeDiscipline.forEach(function(el) {
            htmlOutput += el + ' ';
        });
    }
    if (nodeYear || nodeInstitution || nodeDiscipline) {
        htmlOutput += '</h4>';
    } 
    htmlOutput += '</div>';
    if (nodeQuote || nodeExtendedQuote) {
        htmlOutput += '<div class="single-fellow__quote"><blockquote>&ldquo;';
        htmlOutput += (nodeExtendedQuote) ? nodeExtendedQuote : nodeQuote;
        htmlOutput += '&rdquo;</blockquote></div>';
    }
    if (nodeHighlightStory) {
        htmlOutput += '<hr><div class="single-fellow__highlight-story"><p>' + nodeHighlightStory + '</p></div>';
    }
    if (nodeBio) {
        htmlOutput += '<hr><div class="single-fellow__bio"><strong>Research Summary</strong><br />' + nodeBio + '</div>';
    }
    htmlOutput += '</div>';

    document.querySelector("#fellow-overlay .container").innerHTML = htmlOutput;

    // Add tour navigation if active
    if (tourActive) {
        for (var i = 0; i < tourNodes.length; i++) {
            if (tourNodes[i].userData.slug === nodeSlug) {
                // console.log(i);
                if (i > 0 ) {
                    var prevLink;
                    if (document.getElementById('prev-link')) {
                        prevLink = document.getElementById('prev-link');
                    } else {
                        prevLink = document.createElement('a');
                        prevLink.className = 'tour-nav';
                        prevLink.id = 'prev-link';
                        prevLink.innerHTML = '<svg width="64px" height="64px" viewBox="0 0 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-50.000000, -394.000000)" fill="#FFFFFF"><path d="M82,394 C77.583312,394 73.427104,394.833328 69.531248,396.5 C65.6354,398.166672 62.2396,400.447904 59.343752,403.343752 C56.4479024,406.2396 54.1666752,409.6354 52.5,413.531248 C50.8333248,417.427104 50,421.583312 50,426 C50,430.416688 50.8333248,434.572896 52.5,438.468752 C54.1666752,442.3646 56.4479024,445.7604 59.343752,448.656248 C62.2396,451.552098 65.6354,453.833325 69.531248,455.5 C73.427104,457.166675 77.583312,458 82,458 C86.416688,458 90.572896,457.166675 94.468752,455.5 C98.3646,453.833325 101.7604,451.552098 104.656248,448.656248 C107.552096,445.7604 109.833328,442.3646 111.5,438.468752 C113.166672,434.572896 114,430.416688 114,426 C114,421.583312 113.166672,417.427104 111.5,413.531248 C109.833328,409.6354 107.552096,406.2396 104.656248,403.343752 C101.7604,400.447904 98.3646,398.166672 94.468752,396.5 C90.572896,394.833328 86.416688,394 82,394 Z M95.062496,427.75 L83.75,439.062496 C83.375,439.479168 82.906256,439.687504 82.343752,439.687504 C81.781248,439.687504 81.312504,439.479168 80.937504,439.062496 C80.520832,438.687496 80.312496,438.218752 80.312496,437.656248 C80.312496,437.093744 80.520832,436.625 80.937504,436.25 L89.187504,428 L68,428 C67.458328,428 66.989584,427.802088 66.593752,427.406248 C66.197912,427.010416 66,426.541672 66,426 C66,425.458328 66.197912,424.989584 66.593752,424.593752 C66.989584,424.197912 67.458328,424 68,424 L89.187504,424 L80.937504,415.75 C80.520832,415.375 80.312496,414.906256 80.312496,414.343752 C80.312496,413.781248 80.520832,413.312504 80.937504,412.937504 C81.312504,412.520832 81.781248,412.312496 82.343752,412.312496 C82.906256,412.312496 83.375,412.520832 83.75,412.937504 L95.062496,424.25 C95.312504,424.5 95.479168,424.781248 95.562496,425.093752 C95.645832,425.406248 95.645832,425.708328 95.562496,426 C95.645832,426.291672 95.645832,426.593752 95.562496,426.906248 C95.479168,427.218752 95.312504,427.5 95.062496,427.75 Z" id="Fill-1-Copy" transform="translate(82.000000, 426.000000) rotate(180.000000) translate(-82.000000, -426.000000) "></path></g></g></svg>';
                    }
                    
                    prevLink.href = '#' + tourNodes[i-1].userData.slug;
                    document.getElementById('fellow-overlay').appendChild(prevLink);
                } else {
                    if (document.getElementById('prev-link')) {
                        prevLink = document.getElementById('prev-link');
                        document.getElementById('fellow-overlay').removeChild(prevLink);
                    } 
                }

                if (i < (tourNodes.length - 1) ) {
                    var nextLink;
                    if (document.getElementById('next-link')) {
                        nextLink = document.getElementById('next-link');
                    } else {
                        nextLink = document.createElement('a');
                        nextLink.className = 'tour-nav';
                        nextLink.id = 'next-link';
                        nextLink.innerHTML = '<svg width="64px" height="64px" viewBox="0 0 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-50.000000, -394.000000)" fill="#FFFFFF"><path d="M82,394 C77.583312,394 73.427104,394.833328 69.531248,396.5 C65.6354,398.166672 62.2396,400.447904 59.343752,403.343752 C56.4479024,406.2396 54.1666752,409.6354 52.5,413.531248 C50.8333248,417.427104 50,421.583312 50,426 C50,430.416688 50.8333248,434.572896 52.5,438.468752 C54.1666752,442.3646 56.4479024,445.7604 59.343752,448.656248 C62.2396,451.552098 65.6354,453.833325 69.531248,455.5 C73.427104,457.166675 77.583312,458 82,458 C86.416688,458 90.572896,457.166675 94.468752,455.5 C98.3646,453.833325 101.7604,451.552098 104.656248,448.656248 C107.552096,445.7604 109.833328,442.3646 111.5,438.468752 C113.166672,434.572896 114,430.416688 114,426 C114,421.583312 113.166672,417.427104 111.5,413.531248 C109.833328,409.6354 107.552096,406.2396 104.656248,403.343752 C101.7604,400.447904 98.3646,398.166672 94.468752,396.5 C90.572896,394.833328 86.416688,394 82,394 Z M95.062496,427.75 L83.75,439.062496 C83.375,439.479168 82.906256,439.687504 82.343752,439.687504 C81.781248,439.687504 81.312504,439.479168 80.937504,439.062496 C80.520832,438.687496 80.312496,438.218752 80.312496,437.656248 C80.312496,437.093744 80.520832,436.625 80.937504,436.25 L89.187504,428 L68,428 C67.458328,428 66.989584,427.802088 66.593752,427.406248 C66.197912,427.010416 66,426.541672 66,426 C66,425.458328 66.197912,424.989584 66.593752,424.593752 C66.989584,424.197912 67.458328,424 68,424 L89.187504,424 L80.937504,415.75 C80.520832,415.375 80.312496,414.906256 80.312496,414.343752 C80.312496,413.781248 80.520832,413.312504 80.937504,412.937504 C81.312504,412.520832 81.781248,412.312496 82.343752,412.312496 C82.906256,412.312496 83.375,412.520832 83.75,412.937504 L95.062496,424.25 C95.312504,424.5 95.479168,424.781248 95.562496,425.093752 C95.645832,425.406248 95.645832,425.708328 95.562496,426 C95.645832,426.291672 95.645832,426.593752 95.562496,426.906248 C95.479168,427.218752 95.312504,427.5 95.062496,427.75 Z" id="Fill-1-Copy" transform="translate(82.000000, 426.000000) rotate(180.000000) translate(-82.000000, -426.000000) "></path></g></g></svg>';
                    }
                
                    nextLink.href = '#' + tourNodes[i+1].userData.slug;
                    document.getElementById('fellow-overlay').appendChild(nextLink);
                } else {
                    if (document.getElementById('next-link')) {
                        nextLink = document.getElementById('next-link');
                        document.getElementById('fellow-overlay').removeChild(nextLink);
                    } 
                }
            }
        }   
    }
}

// Moves camera to original position
function cameraReset() {
    var moveCameraPosition = new TWEEN.Tween( camera.position ).to( {
            x: 0,
            y: 0,
            z: 1200}, 2000 )
        .easing( TWEEN.Easing.Quadratic.InOut).start()
        .onComplete(function(){
            // Debugging
            // console.log(camera.position);  
        });
        
    moveCameraPosition.start();
    return moveCameraPosition;       
}

// If the page loads with a hash, fetch that fellow
function showHashNode() {
    if (window.location.hash) {
        var key = window.location.hash.slice(1);
        
        nodeCluster.children.forEach(function(el) {
            if ( el.userData.slug === key ) {               
                pickedNode = el;
                // Universal Analytics
                gtag('event', 'Load', {
                    'event_category' : 'Fellows',
                    'event_label' : el.userData.name
                });
                // Google Analytics 4
                gtag('event', 'Fellows', {
                    'Load' : el.userData.name
                });
                populateNodeOverlay(el);				
                cameraMove(el);
                pickedNodeMove(el);
                randomNodeDispersal(el);
            }    
        });   
    }
}

// Allows hash links with fellow slug to animate to fellow node
function hashLinks(e) {
    var href = this.getAttribute('href');
    if (href.startsWith('#')) {
        e.preventDefault();
        // console.log(href);
        var elements = document.querySelectorAll('.overlay');
        elements.forEach(function(el) {
            if (el.classList.contains('overlay--open')) {
                el.classList.remove('overlay--open');
            }
        });
        var key = href.substring(1);
    
        nodeCluster.children.forEach(function(el) {
            if ( el.userData.slug === key ) {
                // Fades back in the current node that the user is leaving
                if (pickedNode) {
                    fadeMesh(pickedNode, 'in');
                }
                // Sets picked node to the new node the user is going to from the hash link
                pickedNode = el;
                // Universal Analytics
                gtag('event', 'Fellow Click', {
                    'event_category' : 'Fellows',
                    'event_label' : el.userData.name
                });
                // Google Analytics 4
                gtag('event', 'Fellows', {
                    'Fellow Click' : el.userData.name
                });
                populateNodeOverlay(el);
                // console.log(fellowOverlayActive);
                if (fellowOverlayActive) {
                    fellowOverlayCameraMove();

                    setTimeout(function() { 
                        pickedNodeMove(el);
                    }, 500);       
                } else {
                    cameraMove(el);
                    pickedNodeMove(el);
                }			
               
                randomNodeDispersal(el);
                addHashUrl(el);
            }    
        }); 
    }
}

// Adds hash to url when a node is clicked
function addHashUrl(el) {
    var hash = el.userData.slug;

    if (history.pushState) {
        history.pushState(null, null, '#' + hash); 
    } else {
        window.location.hash = '#' + hash;
    }   
}

// Removes hash url
function removeHashUrl() {
    window.location.hash = '';
}

// Creates text label for node
function createTextLabel(node) {
    var textDiv = document.createElement('div');
    textDiv.className = 'node-label';
    textDiv.id = 'label-' + node.userData.slug;
    textDiv.style.position = 'absolute';
    textDiv.style.width = 100;
    textDiv.style.height = 100;
    textDiv.innerHTML = '<h2>' + node.userData.name + '</h2>';
    textDiv.innerHTML += '<h4>' + node.userData.year + '</h4>';
    textDiv.innerHTML += '<h4>' + node.userData.institution + '</h4>';
    if ( node.userData.quote ) textDiv.innerHTML += '<blockquote>&ldquo;' + node.userData.quote + '&rdquo;</blockquote>';
    // Puts label off the page on initial load so they don't show up in a weird spot before being properly postitioned
    textDiv.style.top = -1000;
    textDiv.style.left = -1000;

    var textLabel = {
        element: textDiv,
        parent: node,
        position: new THREE.Vector3(0,0,0)
    };

    return textLabel;
}

// Remove all text label elements from the scene (DOM elements and array for positioning)
function removeTextLabels() {
    textLabels = [];
    var nodeLabels = document.querySelectorAll('.node-label');

    if (nodeLabels !== null && nodeLabels.length > 0) {
        nodeLabels.forEach(function(el) {
            el.remove();
        });
    }
}

function checkCameraDistance(node) {
    if (node.position.distanceTo(camera.position) < 250 ) {
        return true;
    } else {
        return false;
    }
}

function closeIntro() {
    // Go directly to node if hash present in URL
    if (window.location.hash) {
        showHashNode();
    } else {
        cameraIntro();
    }
    
    document.querySelector('body').className = 'scene-entered';
    document.querySelector('.intro-overlay').className += ' intro-overlay--closed';
}

function stopInfoPulse() {
    if (!getCookie('packard_fellows_info_clicked')) {
        document.querySelector('.info').className += ' info--stopped';
        setCookie('packard_fellows_info_clicked', 'true', 7);
    } 
}

/*window.addEventListener('wheel', function(e) {
    if (introActive) {
        closeIntro();
        introActive = false;
    }    
});*/
document.addEventListener("click", function(e) {
    if (introActive) {
        closeIntro();
        introActive = false;
    }    
});
document.addEventListener("touchstart", function(e) {
    if (introActive) {
        closeIntro();
        introActive = false;
    }    
});


// Cookies
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + "," + expires + ", path=/";
    // console.log('cookie!');
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(',');
  
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}





// START ANIMATE
function animate() {
    //setTimeout( function() {
        requestAnimationFrame( animate );
    //}, 1000 / 30 );
    
    TWEEN.update();

    render();
    // stats.update();
}
// END ANIMATE

// START RENDER
function render() {
    // Goes through each text label element (in an array) and updates it's position relative to it's respective node
    for(var i=0; i<textLabels.length; i++) {
        textLabels[i].position.copy(textLabels[i].parent.position);
        var vector = textLabels[i].position.project(camera);
        vector.x = (vector.x + 1)/2 * window.innerWidth;
        vector.y = -(vector.y - 1)/2 * window.innerHeight;

        var distanceToCamera = camera.position.distanceTo(textLabels[i].parent.position);

        var labelChangeRate = 1;

        if (textLabels[i].parent.userData.highlightStory) {
            labelChangeRate = 7.5;
        } else if (textLabels[i].parent.userData.quote) {
            labelChangeRate = 2.5;
        }
        
        // position is adjusted to the middle right of node
        textLabels[i].element.style.left = (vector.x + (2 / (distanceToCamera) * (8000 * labelChangeRate))) + 'px';
        textLabels[i].element.style.top = (vector.y) + 'px';
        // textLabels[i].element.style.transform = 'translateY(-50%) scale(' + (((1 / distanceToCamera) * 100) * 3)  + ')';
        // console.log(parent.position);
    }

    // Go through each node
    for(var i=0; i<nodeCluster.children.length; i++) {
        // Set so circles are always facing the camera
        nodeCluster.children[i].rotation.setFromRotationMatrix( camera.matrix );

        // // If a node is close to the camera
        // if (checkCameraDistance(nodeCluster.children[i])) {
        //     // If node doesn't have a text label, set that value to true and add text label
        //     if (nodeCluster.children[i].userData.textLabelActive === false && nodeCluster.children[i].userData.active === true) {
        //         // console.log(nodeCluster.children[i].userData.name);
        //         nodeCluster.children[i].userData.textLabelActive = true;
                
        //         // Add text label
        //         var textLabel = createTextLabel(nodeCluster.children[i]);
        //         textLabels.push(textLabel);
        //         container.appendChild(textLabel.element);
        //         // console.log(textLabels);
        //     }
                    
        // // If node is far from the camera
        // } else {
        //     // If text label is true (has text label)
        //     if (nodeCluster.children[i].userData.textLabelActive === true) {
                
                
        //         // console.log(document.getElementById('label-' + nodeCluster.children[i].userData.slug));
        //         container.removeChild(document.getElementById('label-' + nodeCluster.children[i].userData.slug));

        //         // Set textlabel attribute to false
        //         nodeCluster.children[i].userData.textLabelActive = false;

        //         // Go through the text label array and remove the one relating to this node
        //         for (var j = 0; j < textLabels.length; j++) {
        //             if (textLabels[j].parent.userData.slug === nodeCluster.children[i].userData.slug) {
        //                 textLabels.splice(j,1);
        //                 break;
        //             }
        //         }
        //     }
        // }
    }    
    // controls.target.set(camera.position.x, camera.position.y, camera.position.z - 1200);
    controls.update();
    // console.log(controls.target);
    // nodeCluster.rotation.y += 0.005;
    // starField.rotation.y += 0.000012;
    renderer.render( scene, camera );
}
// END RENDER





// HELPER FUNCTIONS
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

document.getElementById('audio-play').addEventListener('mousedown', function() {
    sound = false;
    document.getElementById('background-audio').pause();
    document.getElementById('audio-play').style.display = 'none';
    document.getElementById('audio-pause').style.display = 'block';
    // Universal Analytics
    gtag('event', 'Click', {
        'event_category' : 'UI',
        'event_label' : 'Audio Pause'
    });
    // Google Analytics 4
    gtag('event', 'UI', {
        'Audio' : 'Pause'
    });
}, false);
document.getElementById('audio-pause').addEventListener('mousedown', function() {
    sound = true;
    document.getElementById('background-audio').play();
    document.getElementById('audio-play').style.display = 'block';
    document.getElementById('audio-pause').style.display = 'none';
    // Universal Analytics
    // Google Analytics 4
    gtag('event', 'UI', {
        'Audio' : 'Play'
    });
}, false);

function audioPlay(effect) {
    if (sound == true) {
        var audioFile;
        switch (effect) {
            case 'intro':
                audioFile = 'audio/intro.mp3';
                break;
            case 'panel-open':
                audioFile = 'audio/panel-open.mp3';
                break;
            case 'panel-close':
                audioFile = 'audio/panel-close.mp3';
                break;
            case 'filter':
                audioFile = 'audio/filter.mp3';
                break;
        }
        var audio = document.getElementById('effect-audio');
        audio.src = audioFile;
        audio.play();
    }
}


window.onhashchange = function() {
    if (window.location.hash != '') {
        showHashNode();
    } else {
        closeOverlay();
    }
};

